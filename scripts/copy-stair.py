from bpy import context, data

print("****************************")
scene = context.scene

cursor = scene.cursor.location
newStair = data.collections.new("newStair")
scene.collection.children.link(newStair) 


for obj in data.collections['Stair'].all_objects:
    obj.select_set(True)
    dupe = obj.copy()
    dupe.data = obj.data.copy()
    newStair.objects.link(dupe)
    
