#!/bin/bash

rsvg-convert -f pdf -w 300 -h 300 -o pdf/floor0.pdf floor-plan-0.svg
rsvg-convert -f pdf -w 300 -h 300 -o pdf/floor1.pdf floor-plan-1.svg
rsvg-convert -f pdf -w 300 -h 300 -o pdf/floor2.pdf floor-plan-2.svg

pdftk pdf/floor0.pdf pdf/floor1.pdf pdf/floor2.pdf output $@
