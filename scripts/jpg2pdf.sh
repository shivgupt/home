#!/bin/bash

for f in "$@"
  do
    name=`basename -s .jpg $f`
    convert $f pdf/$name.pdf
  done
