# Links to windows and door ideas

- BR wall N: [Sliding Egress window 8' x 5'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653)
- BR wall E: [Sliding Egress Window 8' x 5'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653)
- Kitchen E: [Single Hung 4' x 3'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-47-5-in-x-35-5-in-Vinyl-New-Construction-Black-Single-Hung-Window/5005401997)
- Kitchen S: [Sliding Window 8' x 5'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653)
- Dinning S: [Sliding 2 panel door " x 80"](https://www.lowes.com/pd/JELD-WEN-Clear-Glass-White-Vinyl-Right-Hand-Double-Door-Sliding-Patio-Door-with-Screen-and-Pet-Door-Common-72-in-x-80-in-Actual-71-5-in-x-79-5-in/50419006)
- Family Room S: [Sliding Window 5' x 5'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Right-Hand-Black-Sliding-Window-59-1-2-x-59-1-2/5006061643)
- Family Room W: [Sliding Window 5' x 5'](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Right-Hand-Black-Sliding-Window-59-1-2-x-59-1-2/5006061643)
- Washroom First Floor: [Fixed Glass Window 48" x 12"](https://www.lowes.com/pd/Hy-Lite-Rectangle-New-Construction-White-Exterior-Window-Rough-Opening-48-in-x-12-in-Actual-47-5-in-x-11-5-in/1000036809)
- Washroom Second Floor: [Fixed Glass Window 48" x 48"](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-V-4500-FiniShield-Black-47-1-2-x-47-1-2-Fixed-Picture-Window/5013065189)

- Garage Door: [20'x 9'](https://www.menards.com/main/doors-windows-millwork/garage-doors-openers/garage-doors/ideal-door-reg-commercial-white-garage-door/20x9whtc4ss/p-1444433878489-c-12358.htm?tid=4084098601848755259&ipos=2)

- cooktop: [2 element Induction](https://www.lowes.com/pd/Empava-Portable-Countertop-Horizontal-2-Elements-Black-Induction-Cooktop-120V/5013377531)
- shower cubical: [42" x 42"](https://www.lowes.com/pd/DreamLine-French-Corner-Satin-Black-Floor-Square-2-Piece-Corner-Shower-Kit-Actual-74-75-in-x-42-in-x-42-in/1000843098#preview-compare)
- folding glass door makers: [anderson windows and doors](https://www.andersenwindows.com/windows-and-doors/doors/big-doors/)
