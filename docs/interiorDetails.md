
- Basement walls: We do want to insulate the garage walls but let's line the studs with unpainted OSB.
- Basement ceiling: I think the fire code will require we drywall the ceiling w fire-rated drywall & a drywalled ceiling is cool w us.
- Stairway: wood treads + risers and black metal handrail, we have some pictures we can share showing what we have in mind.
- Trim: Minimalist/Modern look. 6" base and 4" casing
- Kitchen Counter top: Quartz, color TBD
- Stovetop: a [2 element Induction cooktop](https://www.lowes.com/pd/Empava-Portable-Countertop-Horizontal-2-Elements-Black-Induction-Cooktop-120V/5013377531) on the island, indicated by a light square on our floor plan.
- Oven: part of the island directly under the stovetop
- Sink: against the wall directly across from the stove
- Dishwasher: none, we'll use our hands.
- Closet Shelving: None, we'll probably put dressers in the closet.
- Bathrooms 
  - Double vanity in both: 60"x 24"
  - Shower cubical: 42" x 42" https://www.lowes.com/pd/DreamLine-French-Corner-Satin-Black-Floor-Square-2-Piece-Corner-Shower-Kit-Actual-74-75-in-x-42-in-x-42-in/1000843098#preview-compare
  - Top floor bathroom is last-priority. We'll skip finishing that until later if we run out of money towards the end




- Quartz
    Pros:
    - Non porous hence easy to keep bacteria free, less prone to stains and no resealing required
    - more sustainable, durable and customizable designs
    Cons:
    - not as heat resistant as granite and need to use heat pads
    - slightly high price
- Granite
    Pros:
    - Super heat resistant, no heat pads needed
    - Cheaper than quartz
    Cons:
    - Porous surface so need to clean spills soon else stained
    - Surface should be resealed once a year for longevity
    - less sustainable
    - Each piece is unique and no customizations in terms of color
    - depending on finish might be expensive if shipped from outside
