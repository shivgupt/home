Window Sizes W x H

| Ref.# | Size W x H | Notes 
| ----- | ---------- | ----------------------------- |
| 1     | 5' x 5'    | Fixed |
| 2     | 8' x 6'    | Preferably operable sliding window. [Ref link](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653) |
| 3     | 8' x 6'    | Preferably operable sliding window. [Ref link](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653) |
| 4     | 4' x 3'    | Preferable operable | 
| 5     | 4' x 3'    | Preferable operable | 
| 6     | 15' x 6.7' | Folding glass door with 5 panels | 
| 7     | 5' x 5'    | Preferable operable | 
| 8     | 5' x 5'    | Preferable operable. [Ref link](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Right-Hand-Black-Sliding-Window-59-1-2-x-59-1-2/5006061643) | 
| 9     | 5' x 5'    | Fixed |
| 10    | 8' x 6'    | Preferably operable sliding window. [Ref link](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653) |
| 11    | 8' x 6'    | Preferably operable sliding window. [Ref link](https://www.lowes.com/pd/JELD-WEN-JELD-WEN-FiniShield-V-4500-Double-Vented-Sliding-Window-95-1-2-x-59-1-2/5006061653) |
